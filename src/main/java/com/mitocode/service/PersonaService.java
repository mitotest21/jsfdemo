package com.mitocode.service;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mitocode.dao.IDAO;
import com.mitocode.model.Persona;

//@Named
//@RequestScoped
public class PersonaService implements IService, Serializable{

	//@Inject
	private IDAO dao;	
	
	/*public PersonaService(IDAO idao) {
		dao = idao;
	}*/
	
	@Override
	public List<Persona> listar() {
		return dao.listar();
	}
}
