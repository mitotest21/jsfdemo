package com.mitocode.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.mitocode.model.Persona;

@Named
@RequestScoped
public class PersonaDAO implements IDAO, Serializable{
	
	@Override
	public List<Persona> listar() {
		List<Persona> lista = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			Persona per = new Persona();
			per.setIdPersona(i);
			per.setNombres("Jaime");
			per.setApellidos("Medina");
			per.setEdad(27);
			lista.add(per);
		}
		
		return lista;
	}

}
