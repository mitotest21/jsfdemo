package com.mitocode.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.mitocode.model.Persona;
import com.mitocode.service.IService;
import com.mitocode.service.PersonaService;

//@Named
//@ViewScoped
public class PersonaBean implements Serializable {

	private List<Persona> lista;
	private Persona persona;
	
	//@Inject
	private IService service;

	public PersonaBean() {
		lista = new ArrayList<>();
		//service = new PersonaService(new PersonaDAO2());
		service = new PersonaService();
		this.listar();		
	}

	public void listar() {
		this.lista = service.listar();
	}

	public void enviar(Persona per) {
		this.persona = per;
	}

	public List<Persona> getLista() {
		return lista;
	}

	public void setLista(List<Persona> lista) {
		this.lista = lista;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
